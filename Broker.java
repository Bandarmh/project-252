
//Command design pattern.
package com.mycompany.cpit252project;

import java.util.ArrayList;
import java.util.List;

public class Broker {

    public void placeOrders(Order order) {
        order.execute();
    }
}



