
package com.mycompany.cpit252project;
//Command design pattern.
public class SellCrypto implements Order { 

	private cryptocurrency abcStock; 

	public SellCrypto(cryptocurrency abcStock){ 
		this.abcStock = abcStock; } 


	public void execute() { 
		abcStock.sell(); } 

}

